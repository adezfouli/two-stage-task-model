import csv
from numexpr.expressions import double


class DataReader:
    def read_experiment_from_CSV(self, address):

        reader = csv.reader(open(address, 'U'), delimiter=',')
        reader.next()
        data = []

        for s in reader:
            data.append(DataPoint(s[0], s[1], double(s[2]), double(s[3]), double(s[4])))
        return data

    def read_parameters_from_CSV(self, address, n_row):

        reader = csv.reader(open(address, 'U'), delimiter=',')
        reader.next() #skip over config
        reader.next() #skip over header

        data = {}

        current_row = 0
        while current_row < n_row:
            s = reader.next()
            base = 10
            params = [double('nan')] * 23
            i = 0
            for i in range(0, 11):
                params[0 + i] = double(s[base + i])
            i += 1
            #seqBasedSecondStage
            params[0 + i] = bool(s[base + i] in ['TRUE', 'True'])
            i += 1
            #SeqBias
            params[0 + i] = double(s[base + i])
            i += 1
            #Intrup-based
            params[0 + i] = bool(s[base + i] in ['TRUE', 'True'])
            i += 1
            #InerruptProp
            params[0 + i] = double(s[base + i])
            i += 1
            #SeqBeta
            params[0 + i] = double(s[base + i])
            i += 1
            #isSequenceWeight
            params[0 + i] = bool(s[base + i] in ['TRUE', 'True'])
            i += 1
            params[0 + i] = double(s[base + i])
            i += 1
            params[0 + i] = double(s[base + i])
            i += 1
            params[0 + i] = double(s[base + i])
            i += 1
            params[0 + i] = double(s[base + i])
            i += 1
            #Ignore wrong discrimination
            params[0 + i] = bool(s[base + i] in ['TRUE', 'True'])
            i += 1
            params[0 + i] = double(s[base + i])
            data[s[0]] = params
            current_row += 1
        return data


class DataPoint:
    def __init__(self, state, action, state_Onset, action_Onset, action_Offset,
                 p_R1=None, p_R2=None, RT_R1=None, RT_R2=None):
        self.state = state
        self.action = action
        self.stateOneSet = state_Onset
        self.actionOnSet = action_Onset
        self.actionOffSet = action_Offset
        self.p_R1 = p_R1
        self.p_R2 = p_R2
        self.RT_R1 = RT_R1
        self.RT_R2 = RT_R2

    def to_array(self):
        return [self.state, self.action, self.stateOneSet, self.actionOnSet, self.actionOffSet, 0, self.p_R1, self.p_R2,
                self.RT_R1, self.RT_R2]

    def __str__(self):
        return self.state + self.action

    def __repr__(self):
        return str(self)


if __name__ == '__main__':
    output = DataReader().read_parameters_from_CSV(
        '../../../data/ID18/to_simulate/map_laplce_ID18__TBi_SPr_MB_SB_SW_ER9K19/map_laplace__ER9K19.csv',
        8)
    pass

