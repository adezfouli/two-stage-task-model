import numpy
from sklearn.neighbors import KernelDensity


class KDEWrapper:
    def __init__(self, kde):
        self.kde = kde

    def logpdf(self, x):
        return self.kde.score_samples(x)[0]

    @staticmethod
    def get_kde_dist(chain):
        return KDEWrapper(KernelDensity().fit(chain[:, numpy.newaxis]))
