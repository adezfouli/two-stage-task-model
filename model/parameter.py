from Queue import Queue
import csv
import math
import nlopt
import numpy

import scipy.stats as stats
import pymc as mc
from sklearn.neighbors import KernelDensity
from model.KDEWrapper import KDEWrapper

from model.model_config import ModelConfig
from util import *


class Parameter:
    def __init__(self):
        pass

    @staticmethod
    def add_header(x):
        header = ['#name', 'model', 'df', 'LL', 'MAP', 'R2', 'nChoices', 'BIC', 'Laplace', 'message']
        return header + x

    @staticmethod
    def write_output(q_list, config, log, output_path):
        check_dir_exists(output_path)
        file_name = open(output_path + log + '.csv', 'wb')
        output = csv.writer(file_name)
        output.writerow(['#config'] + [str(config)])
        for q in q_list:
            if not q.empty():
                output.writerow(q.get())
                out = {}
                while not q.empty():
                    d = q.get()
                    out[d[0]] = d
                out = sorted(out.items())
                for v in out:
                    (a, b) = v
                    output.writerow(b)
        file_name.flush()
        file_name.close()

    @staticmethod
    def prior_to_array(model, config, prior):
        q = Queue()
        prior_info = []
        for i, p in enumerate(prior):
            for d in p:
                prior_info.append(str(d.dist.name) + str(d.args) + str(d.kwds))
            a = Parameter.add_header(model.extractParameters(config, prior_info))
            a[0] = '#prior_' + str(i)
            q.put(a)
        return q

    @staticmethod
    def get_prior(hyper_prior, config):
        (bounds, startingPoints, prior, hyper_params, hyper_latent_params) = \
            Parameter.config_to_parameter(hyper_prior, config, 0, True)
        return prior

    @staticmethod
    def get_individual_prior(hyper_prior, config, N):
        (bounds, startingPoints, prior, hyper_params, hyper_latent_params) = \
            Parameter.config_to_parameter(hyper_prior, config, N, True)
        return hyper_latent_params

    @staticmethod
    def var_bound_start_point(config):
        bounds, starting_points, prior, hyper_params, prior_list = Parameter.config_to_parameter(None, config, 0, False)
        return bounds, starting_points

    @staticmethod
    def get_default_prior(model, config):
        bounds, starting_points, prior, hyper_params, prior_list = Parameter.config_to_parameter(None, config, 0, True)
        return prior

    @staticmethod
    def get_mcmc_prior_hyper(config, N):
        bounds, starting_points, prior, hyper_params, prior_list = Parameter.config_to_parameter(None, config, N, True)
        return hyper_params, prior_list

    @staticmethod
    def add_normal_hyper(name, hyper_params, input_hyper_params, prior_list, N):
        if input_hyper_params is not None:
            return stats.norm(numpy.mean(input_hyper_params['hyper_' + name + '_mu']),
                              numpy.mean(math.sqrt(1. / input_hyper_params['hyper_' +name + '_tau'])))
        mu = mc.Uniform('hyper_' +name + '_mu', lower=-10.0, upper=15.0, value=1.0)
        tau = mc.Uniform('hyper_' +name + '_tau', lower=0.0, upper=100.0, value=1.0)
        for i in range(N):
            variable = mc.Normal('prior_' + name + str(i), mu=mu, tau=tau, value=1.0)
            prior_list[i].append(variable)
        hyper_params.append(mu)
        hyper_params.append(tau)
        return None

    @staticmethod
    def add_unform_01Hyper(name, hyper_params, input_hyper_params, prior_list, N):
        if input_hyper_params is not None:
            bemu = numpy.mean(input_hyper_params['hyper_' +name + '_bmu'])
            bevar = numpy.mean(input_hyper_params['hyper_' +name + '_bvar'])
            #return stats.beta(a=bemu * bevar + 1.0, b=(1.0 - bemu) * bevar + 1.0)
            return stats.norm(bemu, math.sqrt(1. / bevar))
        bmu = mc.Uniform('hyper_' +name + '_bmu', lower=0.0, upper=1.0, value=0.1)  # mu = alpha / (alpha + beta)
        bvar = mc.Uniform('hyper_' +name + '_bvar', lower=0.0, upper=100.0, value=3.)  # v = alpha+beta
        for i in range(N):
            #variable = mc.Beta('prior_' + name + str(i), alpha=bmu * bvar + 1.0, beta=bvar * (1.0 - bmu) + 1.0, value=0.5)
            variable = mc.Normal('prior_01' + name + str(i), mu=bmu, tau=bvar, value=0.5)
            prior_list[i].append(variable)
        hyper_params.append(bmu)
        hyper_params.append(bvar)
        return None

    @staticmethod
    def add_extended_uniform_hyper(name, hyper_params, input_hyper_params, prior_list, N):
        if input_hyper_params is not None:
            return stats.norm(numpy.mean(input_hyper_params['hyper_' +name + '_mu']),
                              math.sqrt(1. / numpy.mean(input_hyper_params['hyper_' +name + '_tau'])))
        mu = mc.Uniform('hyper_' + name + '_mu', lower=-10, upper=15.0, value=1.0)
        tau = mc.Uniform('hyper_' + name + '_tau', lower=0.0, upper=100.0, value=1.0)
        for i in range(N):
            variable = mc.Normal('prior_' + name + str(i), mu=mu, tau=tau, value=1.0)
            prior_list[i].append(variable)
        hyper_params.append(mu)
        hyper_params.append(tau)
        return None


    @staticmethod
    def config_to_parameter(input_hyper_params, config, N, calculate_prior):
        starting_points = []
        bounds = []

        resolution = 0.4
        beta_bound = (0.0, 1.0)
        gamma_bound = (-7.0, 15.0)

        prior = []
        beta_fun = stats.beta(1.1, 1.1)
        gamma_fun = stats.norm(0.0, 5.0)
        normal_fun = stats.norm(0.0, 5.0)
        intr_prior = stats.beta(90, 10)

        hyper_params = []

        prior_list = [[]]
        for i in range(N - 1):
            prior_list += [[]]

        bounds.append(gamma_bound)
        res = 1.0
        for point in drange(0.0 + res, 2.5 - res, res):
            starting_points.append([point])
        if calculate_prior:
            prior_fun = Parameter.add_extended_uniform_hyper('stage1beta' + str(len(prior)),
                                                             hyper_params, input_hyper_params, prior_list, N)
            prior.append(prior_fun or gamma_fun)

        if (ModelConfig.ModelBased in config) or (ModelConfig.ModelFree in config):
            bounds.append(beta_bound)
            res = resolution
            new_points = []
            for points in starting_points:
                for point in drange(0.0 + res, 1.0 - res, res):
                    new_points.append(points + ([point]))
            starting_points = new_points
            if calculate_prior:
                prior_fun = Parameter.add_unform_01Hyper('Q2LR' + str(len(prior)),
                                                         hyper_params, input_hyper_params, prior_list, N)
                prior.append(prior_fun or beta_fun)

        if ModelConfig.SeqBasedSeparateOptionRL in config:
            bounds.append(beta_bound)
            res = resolution
            new_points = []
            for points in starting_points:
                for point in drange(0.0 + res, 1.0 - res, res):
                    new_points.append(points + ([point]))
            starting_points = new_points
            if calculate_prior:
                prior_fun = Parameter.add_unform_01Hyper(ModelConfig.SeqBasedSeparateOptionRL + str(len(prior)),
                                                         hyper_params, input_hyper_params, prior_list, N)
                prior.append(prior_fun or beta_fun)

        if ModelConfig.ModelFreeStage1LR in config:
            bounds.append(beta_bound)
            res = resolution
            new_points = []
            for points in starting_points:
                for point in drange(0.0 + res, 1.0 - res, res):
                    new_points.append(points + ([point]))
            starting_points = new_points
            if calculate_prior:
                prior_fun = Parameter.add_unform_01Hyper(ModelConfig.ModelFreeStage1LR + str(len(prior)),
                                                         hyper_params, input_hyper_params, prior_list, N)
                prior.append(prior_fun or beta_fun)

        if ModelConfig.EligibilityTraces in config:
            bounds.append(beta_bound)
            res = resolution
            new_points = []
            for points in starting_points:
                for point in [0.1]:
                    new_points.append(points + ([point]))
            starting_points = new_points
            if calculate_prior:
                prior_fun = Parameter.add_unform_01Hyper(ModelConfig.EligibilityTraces + str(len(prior)),
                                                         hyper_params, input_hyper_params, prior_list, N)
                prior.append(prior_fun or beta_fun)

        if ModelConfig.ModelFreeModelBasedWeight in config:
            bounds.append(beta_bound)
            res = resolution
            new_points = []
            for points in starting_points:
                for point in drange(0.0 + res, 1.0 - res, res):
                    new_points.append(points + ([point]))
            starting_points = new_points
            if calculate_prior:
                prior_fun = Parameter.add_unform_01Hyper(ModelConfig.ModelFreeModelBasedWeight + str(len(prior)),
                                                         hyper_params, input_hyper_params, prior_list, N)
                prior.append(prior_fun or beta_fun)

        if (ModelConfig.ActionPerseveration in config) or \
                (ModelConfig.SeqPerseverationAsActionPerseveration in config):
            bounds.append(gamma_bound)
            res = resolution
            new_points = []
            for points in starting_points:
                for point in drange(0.0 + res, 1.0 - res, res):
                    new_points.append(points + ([point]))
            starting_points = new_points
            if calculate_prior:
                prior_fun = Parameter.add_normal_hyper(ModelConfig.ActionPerseveration + str(len(prior)),
                                                       hyper_params, input_hyper_params, prior_list, N)
                prior.append(prior_fun or normal_fun)

        if ModelConfig.SeqPerseveration in config:
            bounds.append(gamma_bound)
            res = resolution
            new_points = []
            for points in starting_points:
                for point in drange(0.0 + res, 1.0 - res, res):
                    new_points.append(points + ([point]))
            starting_points = new_points
            if calculate_prior:
                prior_fun = Parameter.add_normal_hyper(ModelConfig.SeqPerseveration + str(len(prior)),
                                                       hyper_params, input_hyper_params, prior_list, N)
                prior.append(prior_fun or normal_fun)

        if ModelConfig.SeqBasedAverageReward in config:
            bounds.append(gamma_bound)
            res = resolution
            new_points = []
            for points in starting_points:
                for point in drange(0.0 + res, 1.0 - res, res):
                    new_points.append(points + ([point]))
            starting_points = new_points
            if calculate_prior:
                prior_fun = Parameter.add_normal_hyper(ModelConfig.SeqBasedAverageReward + str(len(prior)),
                                                       hyper_params, input_hyper_params, prior_list, N)
                prior.append(prior_fun or normal_fun)

        if ModelConfig.SeqBasedAverageReward in config:
            bounds.append(beta_bound)
            res = resolution
            new_points = []
            for points in starting_points:
                for point in drange(0.0 + res, 1.0 - res, res):
                    new_points.append(points + ([point]))
            starting_points = new_points
            if calculate_prior:
                prior_fun = Parameter.add_unform_01Hyper(ModelConfig.SeqBasedAverageReward + str(len(prior)),
                                                         hyper_params, input_hyper_params, prior_list, N)
                prior.append(prior_fun or beta_fun)

        if ModelConfig.SeqBasedBiased in config:
            bounds.append(gamma_bound)
            res = resolution
            new_points = []
            for points in starting_points:
                for point in drange(0.0 + res, 1.0 - res, res):
                    new_points.append(points + ([point]))
            starting_points = new_points
            if calculate_prior:
                prior_fun = Parameter.add_normal_hyper(ModelConfig.SeqBasedBiased + str(len(prior)),
                                                       hyper_params, input_hyper_params, prior_list, N)
                prior.append(prior_fun or normal_fun)

        if ModelConfig.InterBased in config:
            bounds.append(beta_bound)
            res = resolution
            new_points = []
            for points in starting_points:
                for point in drange(0.0 + res, 1.0 - res, res):
                    new_points.append(points + ([point]))
            starting_points = new_points
            if calculate_prior:
                prior_fun = Parameter.add_unform_01Hyper(ModelConfig.InterBased + str(len(prior)),
                                                         hyper_params, input_hyper_params, prior_list, N)
                prior.append(prior_fun or intr_prior)

        if ModelConfig.SeparateSequenceBeta in config:
            bounds.append(gamma_bound)
            res = 1.0
            new_points = []
            for points in starting_points:
                for point in drange(0.0 + res, 2.5 - res, res):
                    new_points.append(points + ([point]))
            starting_points = new_points
            if calculate_prior:
                prior_fun = Parameter.add_extended_uniform_hyper(ModelConfig.SeparateSequenceBeta + str(len(prior)),
                                                                 hyper_params, input_hyper_params, prior_list, N)
                prior.append(prior_fun or gamma_fun)

        if ModelConfig.SequenceWeight in config:
            bounds.append(beta_bound)
            res = resolution
            new_points = []
            for points in starting_points:
                for point in drange(0.0 + res, 1.0 - res, res):
                    new_points.append(points + ([point]))
            starting_points = new_points
            if calculate_prior:
                prior_fun = Parameter.add_unform_01Hyper(ModelConfig.SequenceWeight + str(len(prior)),
                                                         hyper_params, input_hyper_params, prior_list, N)
                prior.append(prior_fun or beta_fun)

        if ModelConfig.SepSecondStageBeta in config:
            bounds.append(gamma_bound)
            res = 1.
            new_points = []
            for points in starting_points:
                for point in drange(0.0 + res, 2.5 - res, res):
                    new_points.append(points + ([point]))
            starting_points = new_points
            if calculate_prior:
                prior_fun = Parameter.add_extended_uniform_hyper(ModelConfig.SepSecondStageBeta + str(len(prior)),
                                                                 hyper_params, input_hyper_params, prior_list, N)
                prior.append(prior_fun or gamma_fun)

        if ModelConfig.DiscriminationBaseline in config:
            bounds.append(gamma_bound)
            res = .7
            new_points = []
            for points in starting_points:
                for point in drange(0.0 + res, 3. - res, res):
                    new_points.append(points + ([point]))
            starting_points = new_points
            if calculate_prior:
                prior_fun = Parameter.add_extended_uniform_hyper(ModelConfig.DiscriminationBaseline + str(len(prior)),
                                                                 hyper_params, input_hyper_params, prior_list, N)
                prior.append(prior_fun or stats.norm(1, 2))

        if ModelConfig.TransitionBias in config:
            bounds.append(gamma_bound)
            res = .7
            new_points = []
            for points in starting_points:
                for point in drange(-1.0 + res, 2. - res, res):
                    new_points.append(points + ([point]))
            starting_points = new_points
            if calculate_prior:
                prior_fun = Parameter.add_normal_hyper(ModelConfig.TransitionBias + str(len(prior)),
                                                       hyper_params, input_hyper_params, prior_list, N)
                prior.append(prior_fun or stats.norm(1, 2))

        if ModelConfig.DynamicStage1Transition in config:
            bounds.append(beta_bound)
            res = resolution
            new_points = []
            for points in starting_points:
                for point in drange(0.0 + res, 1.0 - res, res):
                    new_points.append(points + ([point]))
            starting_points = new_points
            if calculate_prior:
                prior_fun = Parameter.add_unform_01Hyper(ModelConfig.DynamicStage1Transition + str(len(prior)),
                                                         hyper_params, input_hyper_params, prior_list, N)
                prior.append(prior_fun or beta_fun)

        return bounds, starting_points, prior, hyper_params, prior_list


