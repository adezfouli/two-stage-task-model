__author__ = 'AT'

from Queue import Queue
import emcee
import nlopt
from scipy.stats import gaussian_kde, scipy
from sklearn.neighbors import KernelDensity
from sympy.mpmath import linspace
from model import learning_model
from model.model_config import ModelConfig
from model.parameter import Parameter
from model.prior_estimator import PriorEstimator
from model.util import id_generator, check_dir_exists
import numpy as np
import pymc as mc
import matplotlib.pyplot as plt


class ModelLikelihood:
    def __init__(self, model):
        self.model = model

    def __call__(self, params):
        for val, var in zip(params, self.model.stochastics):
            if not PriorEstimator.check_bound(var, val):
                return -np.inf
            var.value = val
        return self.model.logp


class MCMC_Prior:

    @staticmethod
    def emcee_sample(model):

        # emcee parameters
        ndim = len(model.stochastics)
        nwalkers = 200
        burn = 500
        iter = 2000
        ntemps = 1
        thin = 1
        is_map = True

        p0, MAP = PriorEstimator.get_starting_points(model, ndim, nwalkers, is_map)

        # instantiate sampler passing in the pymc likelihood function
        sampler = emcee.EnsembleSampler(nwalkers, ndim, ModelLikelihood(model))

        pos, prob, state = sampler.run_mcmc(p0, burn)
        sampler.reset()

        sampler.run_mcmc(pos, iter)

        emcee_results = {'#mc_spec': {'nwalkers': nwalkers, 'burn': burn, 'iter': iter, 'thin': thin,
                                      'ntemp': ntemps, 'is map': is_map}, '#starting_point': {}}

        for var, val in zip(model.stochastics, MAP):
            emcee_results['#starting_point'][str(var)] = val

        emcee_results['#chains'] = {}

        for i, var in enumerate(model.stochastics):
            emcee_results['#chains'][str(var)] = sampler.flatchain[:, i]

        emcee_results['#acceptance_fraction'] = np.mean(sampler.acceptance_fraction)
        print 'group parameter estimate finished'
        return emcee_results

    @staticmethod
    def testMCMC():
        config = [
            ModelConfig.ModelFree
        ]

        model = learning_model.LearningModel
        path = '../../../data/ID22/test/'
        # making simulations
        data, params = PriorEstimator.generate_data(config, model, path)

        print 'simulation file loaded'
        print 'mcmc started'
        mcmc_results = PriorEstimator.group_parameter_estimate(data, model, config)
        print 'mcmc finished'
        qmcmc = PriorEstimator.log_mcmc(mcmc_results, config)
        qc = Queue()
        qc.put(['#true parameters'])
        qc.put(Parameter.add_header(model.paramNames()))
        qc.put(Parameter.add_header(params))

        simulation_id = id_generator(4)
        full_output_path = path + 'mcmc_laplace_' + ModelConfig.get_id_config(config, simulation_id) + '/'
        Parameter.write_output(qmcmc + [qc], config, 'fit', full_output_path)

    @staticmethod
    def log_mcmc(mc_results, config):
        q_v = Queue()
        q_v.put(['#mcmc variable', 'mean', 'sd', 'percentiles (16, 50, 84)', 'n'])
        for k, v in mc_results['#chains'].iteritems():
            q_v.put(
                [k, np.mean(v), np.std(v), np.percentile(v, [16, 50, 84], axis=0), len(v)])
        q_m = Queue()
        q_m.put(['#model_spect', mc_results['#mc_spec']])
        q_m.put(['#acceptance_fraction', mc_results['#acceptance_fraction']])

        q_b = Queue()
        q_b.put(['#mcmc starting point'])
        for k, v in mc_results['#starting_point'].iteritems():
            q_b.put([k, v])

        # q_b.put(Parameter.add_header(model.paramNames()))
        # prior_info = []
        # for d in Parameter.get_prior(mcmc_model.stats(), config):
        # prior_info.append(str(d.dist.name) + str(d.args) + str(d.kwds))
        # q_b.put(Parameter.add_header(model.extractParameters(config, prior_info)))

        q_s = Queue()
        # q_s.put(['#gelman_rubin', 'raftery_lewis'])
        # q_s.put([mc_results['#gelman_rubin'], 'na'])
        return [q_v, q_b, q_m, q_s]

    @staticmethod
    def get_kde(model, chains):
        kdes = {}
        for var, chain in zip(model.stochastics, chains):
            kdes[str(var)] = gaussian_kde(chain)
        return kdes

    @staticmethod
    def plotModel(mcmc_model, path, file_name):
        dir_name = path
        check_dir_exists(dir_name)
        mc.Matplot.plot(mcmc_model, format='pdf', path=dir_name,
                        suffix=file_name)

    @staticmethod
    def metropolis_hasting_sample(model):
        iter = 4000
        burn = 2000
        thin = 2
        N = 4
        for i in range(N):
            model.sample(iter=iter, burn=burn, thin=thin)
        return model

    @staticmethod
    def plot_kde(kde):
        x_grid = linspace(-5., 5., 300)
        pdf = kde.evaluate(x_grid)
        plt.plot(x_grid, pdf)
        # plt.show()


    @staticmethod
    def get_starting_points(model, ndim, nwalkers, MAP):
        if MAP:
            f, start = PriorEstimator.get_map(model)
            rnd_generator = scipy.stats.uniform(-0.1, 0.1)

        else:
            start = [0.0] * ndim
            rnd_generator = scipy.stats.uniform(-.1, 6.0)
        p0 = []
        n_point = 0

        while n_point < nwalkers:
            new_point = [0] * ndim
            for i, var in enumerate(model.stochastics):
                found = False
                while not found:
                    c = rnd_generator.rvs() + start[i]
                    if PriorEstimator.check_bound(var, c):
                        found = True
                        new_point[i] = c
            p0.append(new_point)
            n_point += 1
        return p0, start

    @staticmethod
    def group_parameter_estimate(data, model, config):
        print 'group parameter estimate started'
        model = PriorEstimator.build_model(config, data, model)
        # return MCMCEstimate.metropolis_hasting_sample(model)
        return PriorEstimator.emcee_sample(model)


    @staticmethod
    def get_EB_prior(data, model, config):
        mcmc_samples = PriorEstimator.group_parameter_estimate(data, model, config)
        keys = []
        samples = []
        for k, v in mcmc_samples['#chains'].iteritems():
            if k.startswith('hyper_'):
                keys.append(k)
                samples.append(v)

        x = np.transpose(np.array(samples))
        kde = KernelDensity(bandwidth=3.0).fit(x)

        def f(x, g):
            return -kde.score_samples(x)[0]

        lbs = [-np.inf]*len(keys)
        ubs = [np.inf]*len(keys)


        local_opt = nlopt.opt(nlopt.LN_BOBYQA, len(lbs))
        local_opt.set_lower_bounds(lbs)
        local_opt.set_upper_bounds(ubs)
        local_opt.set_min_objective(f)
        local_opt.set_xtol_rel(1e-6)
        x = local_opt.optimize([0]*len(keys))

        input_priors = {}
        for k, v in zip(keys, x):
            input_priors[k] = [v]

        (bounds, startingPoints, prior, hyper_params, hyper_latent_params) = \
            Parameter.config_to_parameter(input_priors, config, len(data), True)
        return [prior] * len(data), mcmc_samples