import multiprocessing

import nlopt
from numpy.linalg import det
from numpy import *
from DerApproximator import get_d2, get_d1

from environment import Environment
from model.parameter import Parameter


class Estimator(multiprocessing.Process):
    def __init__(self, q, name, data, model, config, prior, method, simulation_output_path=''):
        multiprocessing.Process.__init__(self)
        self.data = data
        self.modelClass = model
        self.q = q
        self.name = name
        self.config = config
        self.method = method
        self.simulation_output_path = simulation_output_path
        self.prior = prior
        if prior is None and method != 'bic':
            raise Exception('a prior over parameters is required')
        if method == 'bic' and prior is not None:
            raise Exception('prior is not required with BIC')

    def optimize(self, bound, fit_function, point):
        def fitGradFunction(x, grad):
            if grad.size > 0:
                grad[:] = array(get_d1(fit_function, array(x), stencil=2))
            return fit_function(x)

        lbs = []
        ubs = []
        for x in bound:
            (lb, ub) = x
            lbs.append(lb)
            ubs.append(ub)
        opt = nlopt.opt(nlopt.GN_CRS2_LM, len(lbs))
        opt.set_lower_bounds(lbs)
        opt.set_upper_bounds(ubs)
        opt.set_min_objective(fitGradFunction)
        # opt.set_maxtime(10800)
        opt.set_xtol_rel(1e-3)
        x = opt.optimize(point)

        local_opt = nlopt.opt(nlopt.LN_BOBYQA, len(lbs))
        local_opt.set_lower_bounds(lbs)
        local_opt.set_upper_bounds(ubs)
        local_opt.set_min_objective(fitGradFunction)
        local_opt.set_xtol_rel(1.e-4)
        # local_opt.set_maxtime(10800)
        x = local_opt.optimize(x)
        f = local_opt.last_optimum_value()

        return f, x

    @staticmethod
    def evaluate_model(data, modelClass, config, prior, params, method, name, q, MAP_estimate):
        fit_function = Estimator.fit_function(data, modelClass, config, prior)
        env = Environment()
        env.load_data(data)
        model = modelClass.getModel(modelClass, params, config)
        env.simulate_model(model)
        # calculating BIC
        bic = env.NLL + math.log(env.choicesCount) * env.model.freeParametersCount() / 2.0
        msg = None
        # log p(D|param)p(param)
        MAP = None
        laplace = None
        if method == 'laplace':
            MAP = MAP_estimate
            hessian_det = det(
                get_d2(fit_function, params, stencil=3))
            if hessian_det <= 0:
                laplace = bic
                msg = 'bic replaced laplace'
            else:
                laplace = fit_function(params) \
                          - model.freeParametersCount() * math.log(2. * math.pi) / 2 + \
                          math.log(hessian_det) / 2
        q.put([name, model.name(), model.freeParametersCount(), env.NLL, MAP, 1 - env.NLL / env.randomLL,
                    env.nChoices(),
                    bic, laplace, msg] + (model.extractParameters(config, params)))

    def run(self):
        (bound, starting_point) = Parameter.var_bound_start_point(self.config)
        if self.method not in ['laplace', 'bic']:
            raise Exception('method is no supported')

        # defining optimization function
        fit_function = self.fit_function(self.data, self.modelClass, self.config, self.prior)

        # finding optimal params and p(D|param)p(param)
        min_fit, params = self.optimize(bound, fit_function, starting_point[0])

        Estimator.evaluate_model(self.data, self.modelClass, self.config, self.prior, params, self.method, self.name, self.q, min_fit)

        print 'estimation finished for ' + self.name

    @staticmethod
    def fit_function(data, model, config, prior):
        def fit(x, g=[]):
            log_prior = 0
            if not prior is None:
                for fun, v in zip(prior, x):
                    log_prior -= fun.logpdf(v)
            env = Environment()
            env.load_data(data)
            env.simulate_model(model.getModel(model, x, config))

            # print env.LL
            return env.NLL + log_prior

        return fit

