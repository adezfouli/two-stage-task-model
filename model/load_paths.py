import os
from model.data_reader import DataReader
from model.learning_model import LearningModel

ModelClass = LearningModel
all_output_path = '../data/ID18/fit/'
all_input_path = '../data/ID18/data/'
# all_input_path = '../../../data/ID22/test/input/'

# output = '/export/scratch/adez5182/data/ID22/fit/'
# path =   '/export/scratch/adez5182/data/ID22/data/'

def load_data(path):
    data = []
    listing = os.listdir(path)
    listing = sorted(listing)
    for infile in listing:
        if infile.endswith(".csv"):
            data.append(DataReader().read_experiment_from_CSV(path + infile))
    return data

subject_data = load_data(all_input_path)

name = 'ID18_'
