import csv
import os
from model.model_config import ModelConfig

__author__ = 'AT'

class FindMissing:

    @staticmethod
    def find_missing(path, ref_list):
        listing = os.listdir(path)
        listing = sorted(listing)
        curr_list = []
        for infile in listing:
            if infile.endswith(".csv"):
                reader = csv.reader(open(path + infile, 'U'), delimiter=',')
                curr_list.append(reader.next()[1].replace(' ', '').\
                    replace('[', '').replace(']', '').replace('\'', '').split(','))
        return FindMissing.compare(ref_list, curr_list)
    @staticmethod
    def compare(list1, list2):
        missing = []
        for l1 in list1:
            found = False
            for l2 in list2:
                if set(l1) == set(l2):
                    found = True
            if not found:
                missing.append(l1)
        return missing

if __name__ == '__main__':
    print FindMissing.find_missing('../../../results/raw-laplace-map-unlimited optimization time/set3/tmp/', ModelConfig().get_total_config())
