from Queue import Queue

import nlopt
import pymc as mc
import scipy.stats as stats

from model import learning_model
from model.data_reader import DataReader
from model.model_config import ModelConfig
from model.simulation_env import SimulationEnv
from model.parameter import Parameter
from model.environment import Environment


class PriorEstimator:

    def __init__(self, data, model, config):
        self.data = data
        self.model = model
        self.config = config

    @staticmethod
    def build_model(data, model, config):
        (hyper_params, mc_prior) = Parameter.get_mcmc_prior_hyper(config, len(data))
        fit_function = PriorEstimator.group_fit_function(data, model, config, None)

        @mc.potential
        def custom_stochastic(params=mc_prior):
            return -fit_function(params)

        flatten_prior = [item for sublist in mc_prior for item in sublist]
        model = mc.Model(hyper_params + flatten_prior + [custom_stochastic])
        return model


    @staticmethod
    def get_map_prior(data, model, config):
        model = PriorEstimator.build_model(data, model, config)
        f, x = PriorEstimator.get_map(model)
        input_priors={}
        for var, val in zip(model.stochastics, x):
            input_priors[str(var)] = val
            print var, val
        (bounds, startingPoints, prior, hyper_params, hyper_latent_params) = \
            Parameter.config_to_parameter(input_priors, config, len(data), True)

        q = PriorEstimator.stochastic_to_log(model)

        return [prior] * len(data), q

    @staticmethod
    def check_bound(var, value):
        if value <= 0 and var.__class__.__name__ == 'Gamma':
            return False
        if var.__class__.__name__ == 'Beta' and (value <= 0 or value >= 1):
            return False
        if var.__class__.__name__ == 'Uniform':
            if var.parents['upper'] < value or value < var.parents['lower']:
                return False
        return True

    @staticmethod
    def get_map(model):
        def fitGradFunction(x, grad):
            if grad.size > 0:
                raise Exception('gradient optimization is not supported')
            for var, val in zip(model.stochastics, x):
                var.value = val
            return -model.logp

        lbs = []
        ubs = []
        epsilon = 1.0e-2
        for var in model.stochastics:
            if var.__class__.__name__ == 'Uniform':
                lbs.append(var.parents['lower'] + epsilon)
                ubs.append(var.parents['upper'] - epsilon)
            elif var.__class__.__name__ == 'Beta' or ('prior_01' in str(var)):
                lbs.append(epsilon)
                ubs.append(1.0 - epsilon)
            elif var.__class__.__name__ == 'Gamma':
                lbs.append(epsilon)
                ubs.append(5.0)
            else:
                lbs.append(-5.0)
                ubs.append(20.0)

        global_tol = 1.0e-2
        local_tol = 1.0e-4
        x = [0.2] * len(lbs)
        print 'optimization', 'global tol', global_tol, 'local tol', local_tol

        opt = nlopt.opt(nlopt.GN_CRS2_LM, len(lbs))
        opt.set_lower_bounds(lbs)
        opt.set_upper_bounds(ubs)
        opt.set_min_objective(fitGradFunction)
        opt.set_xtol_rel(global_tol)
        # opt.set_maxtime(10800)
        print 'global optimizer', opt.get_algorithm_name()

        x = opt.optimize(x)

        print 'global optimization finished'

        local_opt = nlopt.opt(nlopt.LN_BOBYQA, len(lbs))
        local_opt.set_lower_bounds(lbs)
        local_opt.set_upper_bounds(ubs)
        local_opt.set_min_objective(fitGradFunction)
        local_opt.set_xtol_rel(local_tol)
        # local_opt.set_maxtime(10800)
        x = local_opt.optimize(x)
        f = local_opt.last_optimum_value()

        print 'local optimization finished', local_opt.get_algorithm_name()

        return f, x

    @staticmethod
    def group_fit_function(data, model, config, prior):
        def fit(x, g=[]):
            logPrior = 0
            if not prior is None:
                for fun, v in zip(prior, x):
                    logPrior -= fun.logpdf(v)
            for i in range(len(data)):
                env = Environment()
                env.load_data(data[i])
                env.simulate_model(model.getModel(model, x[i], config))
                logPrior = logPrior + env.NLL
            return logPrior

        return fit

    @staticmethod
    def stochastic_to_log(model):
        q_b = Queue()
        q_b.put(['#group map point'])
        for v in model.stochastics:
            q_b.put([str(v), v.value])
        q_b.put(['logp', model.logp])
        return q_b

    @staticmethod
    def generate_data(config, model, path):
        b = stats.beta(a=19., b=5.)
        data = []
        for i in range(8):
            rvs = b.rvs()
            rvs = 0.2
            betas = 3.0
            params = [betas,  # beta
                      0.0,  # transitionLR
                      0.0,  # optionLR
                      rvs,  # Q1LR
                      rvs,  # Q2LR
                      0.0,  # ET
                      0.0,  # w
                      0.0,  # actionPerseveration
                      0.0,  # seqPerseveration
                      0.0,  # sequenceValueCoef
                      0.0,  # averageRewardLR
                      False,  # seqBasedSecondStage
                      0.0,  # SeqBias
                      False,  # Intrup-based
                      0.0,  # InerruptProp
                      0.0,  # SeqBeta
                      None,  # isSequenceWeight
                      None,  # SequenceWeight
                      betas,  # Second stage beta
                      0.0,  # Discrimination Baseline
                      0.0,  # Transition bias
                      False,  # Ignore wrong discrimination
                      0.7,  # transition
            ]
            model_instance = model.instantiateFromParams(params, config)
            # loading data from simulation
            simulated_file = SimulationEnv.simulate_fixed_parameters(model_instance,
                                                                     ModelConfig.getFileName(model, config) + '.csv',
                                                                     path, 200)
            data.append(DataReader().read_experiment_from_CSV(simulated_file))

        print 'simulation file generated'
        return data, params


    @staticmethod
    def test_map():
        config = [
            ModelConfig.ModelFree
        ]

        model = learning_model.LearningModel
        path = '../../../data/ID22/test/'
        # making simulations
        data, params = PriorEstimator.generate_data(config, model, path)

        print 'simulation file loaded'
        print 'map started'
        map_results = PriorEstimator.get_map_prior(data, model, config)
        print map_results
        print 'map finished'

        # qc = Queue()
        # qc.put(['#true parameters'])
        # qc.put(Parameter.add_header(model.paramNames()))
        # qc.put(Parameter.add_header(params))
        #
        # simulation_id = id_generator(4)
        # full_output_path = path + 'mcmc_laplace_' + ModelConfig.get_id_config(config, simulation_id) + '/'
        # Parameter.write_output(qmcmc + [qc], config, 'fit', full_output_path)

if __name__ == '__main__':
    PriorEstimator.test_map()