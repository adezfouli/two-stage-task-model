import multiprocessing
from multiprocessing import Manager
import multiprocessing.pool
import os
import random
import psutil

from model import learning_model
from model.model_comparison import ModelComparison

from model.parameter import Parameter
from model.data_reader import DataReader
from model.estimator import Estimator
from model.prior_estimator import PriorEstimator
from model.model_config import ModelConfig
from model.util import id_generator
from Queue import Queue
from load_paths import *
from multi_pool import *


class MultiEstimator:
    @staticmethod
    def individual_estimate(data, model, config, log, output_path, method, prior, multi_thread):
        threads = []
        q = Manager().Queue()
        q.put(Parameter.add_header(model.paramNames()))
        for i in range(len(data)):
            print 'estimation started for: ', name, log, str(i)
            subject_prior = (None if prior is None else prior[i])
            est = Estimator(q, str(i), data[i], model, config, subject_prior, method, output_path)
            if multi_thread:
                est.start()
            else:
                est.run()
            threads.append(est)
        if multi_thread:
            [x.join() for x in threads]
        return q

    @staticmethod
    def individual_estimate_and_export(data, model, config, log, method, output_path, prior, multi_thread):
        q = MultiEstimator.individual_estimate(data=data, model=model, config=config, log=log, output_path=output_path,
                                               method=method, prior=prior, multi_thread=multi_thread)
        q_b = Queue()
        if prior is not None:
            prior_info = []
            for d in prior:
                prior_info.append(str(d.dist.name) + str(d.args) + str(d.kwds))
            q_b.put(Parameter.add_header(model.extractParameters(config, prior_info)))
        Parameter.write_output([q, q_b], config, log, output_path)


    @staticmethod
    def individual_estimate_mcmc_group_prior(data, model, config, log, output_path):
        prior, mcmc_results = PriorEstimator.get_EB_prior(data, model, config, len(data))
        q_prior = Parameter.prior_to_array(model, config, prior)
        q2 = MultiEstimator.individual_estimate(data, model, config, log, output_path, 'laplace', prior, False)
        qmcmc = PriorEstimator.log_mcmc(mcmc_results, config)
        Parameter.write_output([q2] + [q_prior] + qmcmc, config, log, output_path)

    @staticmethod
    def individual_estimate_map_group_prior(data, model, config, log, output_path):
        priors, q = PriorEstimator.get_map_prior(data, model, config)
        q_prior = Parameter.prior_to_array(model, config, priors)
        q2 = MultiEstimator.individual_estimate(data, model, config, log, output_path, 'laplace', priors, MultiEstimator.concurrent())
        Parameter.write_output([q2] + [q_prior] + [q], config, log, output_path)



    @staticmethod
    def concurrent():
        if multiprocessing.cpu_count() * (100. - psutil.cpu_percent(random.random() * 10 + 1)) / 100 > 3:
            return True
        return False


def group_MAP_estimate(config):
    print 'group estimate started for with map laplace started for', len(subject_data), 'subjects'
    print 'config: ' + ''.join(config) + ' started'
    simulation_id = id_generator(6)
    full_output_path = all_output_path + 'map_laplce_' + name + ModelConfig.get_id_config(config, simulation_id) + '/'
    MultiEstimator.individual_estimate_map_group_prior(subject_data, learning_model, config,
                                                       'map_laplace_' + '_' + simulation_id, full_output_path)


def group_EB_estimate(config):
    print 'group estimate'
    print 'config: ' + ''.join(config) + ' started'
    simulation_id = id_generator(6)
    full_output_path = all_output_path + 'mcmc_' + name + ModelConfig.get_id_config(config, simulation_id) + '/'
    MultiEstimator.individual_estimate_mcmc_group_prior(subject_data, learning_model, config, 'mcmc_laplace_' + name,
                                                        full_output_path)


def bic_estimate(config):
    print 'bic estimate'
    print 'config: ' + ''.join(config) + ' started'
    simulation_id = id_generator(6)
    full_output_path = all_output_path + 'bic_' + name + ModelConfig.get_id_config(config, simulation_id) + '/'
    MultiEstimator.individual_estimate_and_export(subject_data, learning_model, config,
                                                  'bic_' + '_' + simulation_id, 'bic', full_output_path, None, MultiEstimator.concurrent())


def laplace_estimate(config, multi_thread=False):
    print 'laplace estimate'
    print 'config: ' + ''.join(config) + ' started'
    simulation_id = id_generator(4)
    full_output_path = all_output_path + 'laplace_' + name + ModelConfig.get_id_config(config, simulation_id) + '/'
    prior = Parameter.get_default_prior(learning_model, config)
    prior = [prior] * len(subject_data)
    MultiEstimator.individual_estimate_and_export(subject_data, learning_model, config,
                                                  'laplace_' + '_' + simulation_id,
                                                  'laplace', full_output_path, prior, multi_thread)




def serial_laplace_estimate(config_list, multi_thread):
    for config in config_list:
        print 'laplace estimate'
        print 'config: ' + ''.join(config) + ' started'
        simulation_id = id_generator(6)
        full_output_path = all_output_path + 'laplace_' + name + ModelConfig.get_id_config(config, simulation_id) + '/'
        prior = Parameter.get_default_prior(learning_model, config)
        MultiEstimator.individual_estimate_and_export(subject_data, learning_model, config,
                                                      'laplace_' + '_' + simulation_id,
                                                      'laplace', full_output_path, prior, multi_thread)
if __name__ == '__main__':
    pool = MyPool(processes=1)
    multi = MultiEstimator()

    # group_MAP_estimate(ModelConfig.get_total_config())
    # group_MAP_estimate(ModelConfig.get_simple_model()[0])
    # pool.map(group_MAP_estimate, (ModelConfig().get_total_config()))
    # print range(0,100)[2:200:3]
    # print range(0,100)[1:100:3]
    # print range(0,100)[2:100:3]
    # print len(ModelConfig().get_mb_or_mf_config()) + len(ModelConfig().get_seq_config())
    # pool.map(bic_estimate, (ModelConfig().get_total_config()[0:84:2]))
    # pool.map(group_MAP_estimate, (ModelConfig().get_total_config()[2:100:3]))
    # print len(ModelConfig().get_total_config())
    # pool.map(group_MAP_estimate, (ModelConfig().get_mb_or_mf_config()))
    # ModelConfig().get_missing_config()
    # pool.map(group_MAP_estimate, (ModelConfig().get_missing_config()))
    # pool.map(group_MAP_estimate, (ModelConfig().get_missing_config())[0:100:3])
    # pool.map(group_MAP_estimate, (ModelConfig().get_missing_config())[1:100:3])
    # pool.map(group_MAP_estimate, (ModelConfig().get_missing_config())[2:100:3])
    # pool.map(bic_estimate, (ModelConfig().get_total_config()))

    ModelComparison.simulate_models( '../../results/to_simulate/', subject_data,

                                             learning_model, all_output_path)
