import os
import profile
import random
import math
from pymc import *
from model.model_config import ModelConfig
from model.data_reader import DataReader, DataPoint
from model.learning_model import LearningModel
from model.environment import Environment
from time import time

class SimulationEnv(Environment):

    def __init__(self, trials):
        Environment.__init__(self)
        self.changeProp = 0.14
        self.S1_A1_prob = 1.0
        self.S1_A2_prob = 0.0
        self.S2_A1_prob = 0.0
        self.S2_A2_prob = 0.0
        self.trials = trials

    def get_state_reward(self, state, action):


        p = random.random()
        r = 0
        to_state = None

        if state == 'S1' and action == 'R1':
            if p < 0.8:
                to_state = 'S2'
            else:
                to_state = 'S3'
        if state == 'S1' and action == 'R2':
            if p < 0.8:
                to_state = 'S3'
            else:
                to_state = 'S2'

        if state == 'S2' and action == 'R1':
            if random.random() < self.S1_A1_prob:
                to_state = 'REWARD'
                r = 1
            else:
                to_state = 'NON_REWARD'
        if state == 'S2' and action == 'R2':
            if random.random() < self.S1_A2_prob:
                to_state = 'REWARD'
                r = 1
            else:
                to_state = 'NON_REWARD'
        if state == 'S3' and action == 'R1':
            if random.random() < self.S2_A1_prob:
                to_state = 'REWARD'
                r = 1
            else:
                to_state = 'NON_REWARD'

        if state == 'S3' and action == 'R2':
            if random.random() < self.S2_A2_prob:
                to_state = 'REWARD'
                r = 1
            else:
                to_state = 'NON_REWARD'

        if state == 'REWARD' or state == 'NON_REWARD':
            to_state = 'BLANK'

        if state == 'BLANK':
            to_state = 'S1'

        if state == 'REWARD' or state == 'NON_REWARD':
            self.current_trial += 1
            if random.random() < self.changeProp:
                tmp = self.S1_A1_prob
                self.S1_A1_prob = self.S2_A2_prob
                self.S2_A2_prob = tmp

        return to_state, r


    def skip_repeated_actions(self):
        pass

    def log_model(self, model):
        data_point = DataPoint(self.state, self.action, 0, 0, 0)
        props, RT = model.action_probabilities(data_point.state)
        if props.has_key('R1'):
            data_point.p_R1 = math.exp(props['R1'])
        if props.has_key('R2'):
            data_point.p_R2 =math.exp(props['R2'])
        if RT.has_key('R1'):
            data_point.RT_R1 = RT['R1']
        if RT.has_key('R2'):
            data_point.RT_R2 = RT['R2']

        self.model_data.append(data_point)
#

    def not_finished(self):
        return self.current_trial < self.trials

    def get_action(self):
        p, rt = self.model.action_probabilities(self.state)
        return self.sample_action(p)


    @staticmethod
    def simulate_files_from_parameters(params, output_path):
        for name, p in params.iteritems():
            print 'simulation started for: ', name
            model = LearningModel.instantiateFromParamsAndModel(True, False, True, None, p)
            env = SimulationEnv(1000)
            env.simulate_model(model)
            env.export_model_data('s'+ str(name) + '.csv', output_path)


    @staticmethod
    def simulate_fixed_parameters(model, name, output, trials=100):
        env = SimulationEnv(trials)
        env.simulate_model(model)
        # return env.export_model_data(name, output + 'input/')

    @staticmethod
    def test():
        config =             [
                ModelConfig.ModelBased,
                # ModelConfig.ModelFree,
                ModelConfig.SeqBased,
            ]


        model = LearningModel
        # making simulations
        params = [5.0,  #beta
                  1.0,  #transitionLR
                  0.4,  #optionLR
                  1.0,  #Q1LR
                  1.0,  #Q2LR
                  1.0,  #ET
                  0.0,  #w
                  0.0,  #actionPerseveration
                  0.0,  #seqPerseveration
                  0.0,  #sequenceValueCoef
                  0.0,  #averageRewardLR
                  True,  #seqBasedSecondStage
                  0.0,  #SeqBias
                  False,  #Intrup-based
                  0.0,  #InerruptProp
                  5.0,  #SeqBeta
                  None,  #isSequenceWeight
                  None,  #SequenceWeight
                  2.0,  #Second stage beta
                  1.0,  #Discrimination Baseline
                  1.0,  #Transition bias
                  False,  #Ignore wrong discrimination
                  0.7,  #transition
        ]
        path = '../../../data/ID22/test/simulation/'
        start_time = time()
        for i in range(500):
            model_instance = model.instantiateFromParams(params, config)
            simulated_file = SimulationEnv.simulate_fixed_parameters(model_instance, 'test' + str(i) + '.csv', path, 300)
        elapsed_time = time() - start_time
        print elapsed_time


if __name__ == '__main__':
    output_path = '../../data/ID18/simulation/'
    SimulationEnv.test()
    # input_file = '../../data/ID18/to_simulate/sim__SPr_S2B_MB_SB_TBi_map_laplace__N01UM5/simmap_laplace__N01UM5.csv'
    # input_file = '../../data/ID18/to_simulate/sim__SPr_S2B_MB_SB_TBi_map_laplace__N01UM5/simmap_laplace__N01UM5.csv'
    # input_file = '../../data/ID18/to_simulate/sim__OpLR_Int_SPr_S2B_SBe_Dsr_MB_SB_TBi_map_laplace__SREK2K/simmap_laplace__SREK2K.csv'
    # input_file = '../../data/ID18/to_simulate/sim__OpLR_Int_SPr_S2B_SBe_Dsr_MB_SB_TBi_map_laplace__SREK2K/simmap_laplace__SREK2K.csv'
    # input_file = '../../data/ID18/to_simulate/sim__SPr_Dsr_MB_SB_TBi_map_laplace__LRZDCU/simmap_laplace__LRZDCU.csv'
    # input_file = '../../data/ID18/to_simulate/bic_ID18__OpLR_Int_SPr_S2B_SBe_Dsr_TBi_MB_SB_0I9EOS/bic__0I9EOS.csv'
    # input_file = '../../data/ID18/to_simulate/sim__SPr_S2B_MB_SB_map_laplace__ILZ58G/simmap_laplace__ILZ58G.csv'

    # SimulationEnv.simulate_files_from_parameters(DataReader().read_parameters_from_CSV(input_file, 8), output_path)