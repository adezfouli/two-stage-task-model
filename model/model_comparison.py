import csv
from multiprocessing import Manager
import os
import math
from scipy import stats
from model.estimator import Estimator
from model.model_config import ModelConfig
from operator import itemgetter
from model.parameter import Parameter
from model.util import id_generator


__author__ = 'AT'


class ModelComparison:

    def __init__(self):
        pass

    @staticmethod
    def add_data(config):
        # return '_MB' not in config
        # return '_MB' in config and '_MF' in config
        # return '_MB' in config and '_MF' not in config and '_SB' not in config
        # return '_Int' not in config and '_Dsr' not in config and '_SPr' not in config
        # return ('_TBi' not in config and '_Dsr' not in config and '_Int' not in config and '_SB' in config) \
        # or ('_TBi' not in config and '_Dsr' not in config and '_Int' not in config and '_MB' not in config) \
        # or  ('_TBi' not in config and '_Dsr' not in config \
        #        and '_Int' not in config and '_MB' in config and '_SB' not in config and '_MF' not in config) \
        # or ('_TBi' not in config and '_Dsr' not in config \
        #        and '_Int' not in config and '_MB' in config and '_SB' not in config and '_MF' in config)
        return True

    @staticmethod
    def compare_models(path, N, factor):
        listing = os.listdir(path)
        listing = sorted(listing)
        all_data = []
        for infile in listing:
            if infile.endswith(".csv"):
                data = ModelComparison.model_evidence(path + infile, infile, N, factor)
                if ModelComparison.add_data(data['config']):
                    all_data.append(data)
        sort_col = 'group_evidence'
        LL_sorted = ModelComparison.sort_by(all_data, sort_col)
        ModelComparison.print_data(LL_sorted, sort_col)
        ModelComparison.write_to_csv(LL_sorted, 'myoutput.csv')

    @staticmethod
    def write_to_csv(data, output):
        with open(output, 'wb') as f:
            w = csv.writer(f)
            w.writerow(data[0].keys())
            for d in data:
                w.writerow(d.values())

    @staticmethod
    def sort_by(data, col):
        def getKey(item):
            return item[col]
        return sorted(data, key=getKey)

    @staticmethod
    def print_data(data, col):
        print 'total n model:', len(data)
        for d in data:
            print d['name'], d[col], d['config']

    @staticmethod
    def parse_config(raw_config):
        return raw_config.replace(' ', ''). \
            replace('[', '').replace(']', '').replace('\'', '').split(',')

    @staticmethod
    def model_evidence(path, name, N, factor='Laplace'):
        reader = csv.reader(open(path, 'U'), delimiter=',')
        props = {'config': None, 'df': 0, 'LL': 0, 'MAP': 0, 'R2': 0, 'nChoices': 0, 'BIC': 0, 'Laplace': 0}
        raw_config = reader.next()[1]
        props['config'] = ModelComparison.parse_config(raw_config)
        props['name'] = name
        reader.next()
        for i in range(N):
            data = reader.next()
            props['df'] += int(float(data[2]))
            props['LL'] += float(data[3])
            # props['MAP'] += float(data[4])
            props['R2'] += float(data[5])
            props['nChoices'] += int(float(data[6]))
            props['BIC'] += (float(data[7]))
            if data[8] != '':
                props['Laplace'] += (float(data[8]))
        props['df'] /= N
        props['R2'] /= N
        if factor == 'Laplace':
            props['group_evidence'] = props[factor] + float(props['df']) * math.log(props['nChoices'])
        if factor == 'BIC':
            props['group_evidence'] = props[factor]
        return props

    @staticmethod
    def simulate_models(path, data, model, outputpath):
        listing = os.listdir(path)
        listing = sorted(listing)
        for infile in listing:
            if infile.endswith(".csv"):
                ModelComparison.simulate_individua_prior(data, path + infile, model, outputpath, infile.split('.')[0])

    @staticmethod
    def read_parameter_from_file(path):
        reader = csv.reader(open(path, 'U'), delimiter=',')
        raw_config = reader.next()[1]
        config = ModelComparison.parse_config(raw_config)
        for i in range(18):
            reader.next()
        hypers = {}
        priors = []
        for r in reader:
            d = r[0].split('_')
            if '' in d:
                d.remove('')
            if len(d) >= 3:
                if d[2] == 'bmu':
                    d[2] = 'mu'
                if d[2] == 'bvar':
                    d[2] = 'tau'
                if d[0] == 'hyper':
                    if d[1] not in hypers.keys():
                        hypers[d[1]] = {d[2]: r[1]}
                    else:
                        hypers[d[1]][d[2]] = r[1]

            if d[0] == 'prior':
                priors.append([d[-1:][0], r[1]])
        return config, ModelComparison.get_hypers(hypers), ModelComparison.get_priors(priors)

    @staticmethod
    def simulate_individua_prior(data, path, model, outputpath, simulation_id):
        config, hypers, priors = ModelComparison.read_parameter_from_file(path)
        q = Manager().Queue()
        q.put(Parameter.add_header(model.paramNames()))

        for i in range(len(data)):
            print 'simulation for data:',i
            Estimator.evaluate_model(data[i], model, config, hypers, priors[i], 'laplace', str(i), q, None)

        Parameter.write_output([q], config, 'sim' + simulation_id, outputpath + 'sim_' + ModelConfig.get_id_config(config, simulation_id)+ '/')

    @staticmethod
    def get_hypers(hyper_raw):
        hypers = []
        for k,v in hyper_raw.iteritems():
            hypers += [[int(k[-1:]), stats.norm(float(v['mu']), math.sqrt(1. / float(v['tau'])))]]

        hypers.sort(key=itemgetter(0))
        sorted_hypers = []
        for v in hypers:
            sorted_hypers.append(v[1])
        return sorted_hypers


    @staticmethod
    def get_priors(prior_raw):
        priors = [[] for j in range(8)]
        for r in prior_raw:
            priors[int(r[0][-1:])].append([int(r[0][-2:-1]), float(r[1])])
        for p in priors:
            p.sort(key=itemgetter(0))
        sorted_priors = [[] for j in range(8)]
        for i, p in enumerate(priors):
            for v in p:
                sorted_priors[i].append(v[1])

        return sorted_priors


if __name__ == '__main__':
    pass
    # ModelComparison.compare_models('../../../results/for-analysis-laplace-set123/', 8, 'Laplace')
    ModelComparison.compare_models('../../../results/for-analysis-bic/', 8, 'BIC')
    # ModelComparison.read_parameter_from_file('../../../results/for-analysis-laplace/map_laplace__1SV1DF.csv')
    # print 'sdfs'[-2:]
