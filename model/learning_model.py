import copy
import math
from numexpr.expressions import double
from model.model_config import ModelConfig
import numpy as np


class LearningModel:
    def __init__(self,
                 model_based,
                 model_free,
                 sequence_based,
                 df,
                 beta,
                 MB_stage2_LR,
                 sequence_LR,
                 MF_stage1_LR,
                 MF_stage2_LR,
                 ET,
                 w,
                 action_perseveration,
                 sequence_perseveration,
                 sequenceValueCoef,
                 averageRewardLR,
                 seqBasedSecondStageProps,
                 seqBias,
                 interrupt_sequence,
                 interrupt_prob,
                 sequence_beta,
                 isSequenceWeight,
                 sequence_weight,
                 stage2_beta,
                 discrimination_baseline,
                 transition_bias,
                 notLearnFromIncorrectDiscrimination,
                 stage1_transition_prop
    ):
        transitionProps = stage1_transition_prop
        rewardProps = 0.0

        self.beta = beta
        self.transitionLR = MB_stage2_LR
        self.optionLR = sequence_LR
        self.seqPerseveration = sequence_perseveration
        self.actionPerseveration = action_perseveration
        self.averageRewardLR = averageRewardLR
        self.sequenceValueCoef = sequenceValueCoef
        self.df = df
        self.Q1LR = MF_stage1_LR
        self.Q2LR = MF_stage2_LR
        self.ET = ET
        self.w = w
        self.modelBased = model_based
        self.modelFree = model_free
        self.seqBased = sequence_based
        self.SeqBasedSecondStageProps = seqBasedSecondStageProps
        self.seqBias = seqBias
        self.InterruptSequence = interrupt_sequence
        self.InterruptProb = interrupt_prob
        self.sequenceBeta = sequence_beta
        self.isSequenceWeight = isSequenceWeight
        self.sequenceWeight = sequence_weight
        self.secondStageBeta = stage2_beta
        self.discriminationBaseline = discrimination_baseline
        self.transitionBias = transition_bias
        self.notLearnFromIncorrectDiscrimination = notLearnFromIncorrectDiscrimination

        self.T = {'S1':
                      {'R1': {'S2': transitionProps, 'S3': 1 - transitionProps},
                       'R2': {'S2': 1 - transitionProps, 'S3': transitionProps},
                      },
                  'S2':
                      {'R1': {'REWARD': rewardProps, 'NON_REWARD': 1 - rewardProps},
                       'R2': {'REWARD': rewardProps, 'NON_REWARD': 1 - rewardProps}
                      },
                  'S3':
                      {'R1': {'REWARD': rewardProps, 'NON_REWARD': 1 - rewardProps},
                       'R2': {'REWARD': rewardProps, 'NON_REWARD': 1 - rewardProps},
                      },
                  'REWARD':
                      {'auto': {'BLANK': 1}},

                  'NON_REWARD':
                      {'auto': {'BLANK': 1}},

                  'BLANK':
                      {'auto': {'S1': 1}}

        }

        if self.isSeqBased():
            self.T['S1']['R1R1'] = {'REWARD': rewardProps, 'NON_REWARD': 1 - rewardProps}
            self.T['S1']['R1R2'] = {'REWARD': rewardProps, 'NON_REWARD': 1 - rewardProps}
            self.T['S1']['R2R1'] = {'REWARD': rewardProps, 'NON_REWARD': 1 - rewardProps}
            self.T['S1']['R2R2'] = {'REWARD': rewardProps, 'NON_REWARD': 1 - rewardProps}

        self.sorted_keys = {}
        for s in self.T.keys():
            self.sorted_keys[s] = sorted(self.T[s].keys())

        self.R = {'REWARD': 1, 'NON_REWARD': 0, 'S1': 0, 'S2': 0, 'S3': 0, 'BLANK': 0}

        self.TerminalStates = ['REWARD', 'NON_REWARD']

        self.lastAction = {'S1': None, 'S2': None, 'S3': None, 'REWARD': None, 'NON_REWARD': None, 'BLANK': None}

        self.Q = {'S1':
                      {'R1': 0.0, 'R2': 0.0},
                  'S2':
                      {'R1': 0.0, 'R2': 0.0},
                  'S3':
                      {'R1': 0.0, 'R2': 0.0},

        }

        self.lastOption = None
        self.averageReward = 0

        self.actionBias = {'S1':
                               {'R1': 0.0, 'R2': 0.0},
                           'S2':
                               {'R1': discrimination_baseline, 'R2': 0.0},
                           'S3':
                               {'R1': 0.0, 'R2': discrimination_baseline},

        }

        self.state_bias = {'S2': 0.0, 'S3': 0.0}

    def modelBasedActionValue(self, state, action):
        if state in self.TerminalStates:
            return self.R[state]

        else:
            value = 0
            for k, v in self.T[state][action].iteritems():
                value += v * self.modelBasedValue(k)
            if self.isSequence(action):
                value += self.sequenceValue()
            return value

    def actionValues(self, state, action):
        if state in self.TerminalStates or state == 'BLANK':
            return self.R[state]

        value = 0

        if self.isModelFree():
            value = (1 - self.w) * self.Q[state][action]

        if self.isModelBased():
            # value += self.w * self.modelBasedActionValue(state, action)
            if state == 'S2' or state == 'S3' or self.isSequence(action):
                value += self.w * self.T[state][action]['REWARD']
            else:
                value += self.w * (
                    self.T[state][action]['S2'] * (max(self.T['S2']['R1']['REWARD'], self.T['S2']['R2']['REWARD'])+ self.state_bias['S2'])
                    + self.T[state][action]['S3'] * (max(self.T['S3']['R1']['REWARD'], self.T['S3']['R2']['REWARD']) + self.state_bias['S3']))
            # if self.isSequence(action):
            #     value += self.w * self.sequenceValue()

        return value

    def isSequence(self, action):
        return action == 'R1R1' or action == 'R1R2' or action == 'R2R1' or action == 'R2R2'

    def sequenceValue(self):
        return self.seqBias + self.averageRewardLR * self.sequenceValueCoef

    def modelBasedValue(self, state):
        if state in self.TerminalStates:
            return self.R[state]

        else:
            values = []
            for k in self.T[state].iterkeys():
                values.append(self.modelBasedActionValue(state, k))
            return max(values)

    def get_beta(self, action, state):
        ws = 1.
        wns = 1.
        if self.isSequenceWeight:
            ws = self.sequenceWeight
            wns = 1 - ws

        if self.isSequence(action):
            return ws * self.sequenceBeta
        else:
            if state == 'S1':
                return wns * self.beta
            else:
                return wns * self.secondStageBeta

    def action_in_softmax(self, action, state):
        return (self.get_beta(action, state) * self.actionValues(state, action) +
                (0.0 if (state != 'S1' or action != self.lastAction[state]) else self.actionPerseveration) +
                self.getActionBias(state, action) +
                (0.0 if (action != self.lastOption) else self.seqPerseveration))

    def action_probabilities(self, state):
        props = {}
        expos = [0] * (len(self.T[state]))

        keys = self.sorted_keys[state]
        values = [0]*(len(keys))
        for k in range(len(keys)):
            values[k] = self.action_in_softmax(keys[k], state)

        for k1 in range(len(values)):
            t = 0
            for k2 in range(len(values)):
                expos[t] = values[k2] - values[k1]
                t += 1

            # for numerical stability
            maxExpos = max(expos)
            sum = 0.0
            for comp in expos:
                sum += math.exp(comp - maxExpos)
            sum = maxExpos + math.log(sum)
            props[keys[k1]] = -sum

        if self.isSeqBased():
            if state == 'S1':
                self.propsInS1 =props.copy()
                max1 = max([props['R1'], props['R1R1'], props['R1R2']])
                props['R1'] = max1 + math.log(math.exp(props['R1'] - max1) +
                                              math.exp(props['R1R1'] - max1) +
                                              math.exp(props['R1R2'] - max1))
                max1 = max([props['R2'], props['R2R1'], props['R2R2']])
                props['R2'] = max1 + math.log(math.exp(props['R2'] - max1) +
                                              math.exp(props['R2R1'] - max1) +
                                              math.exp(props['R2R2'] - max1))
                del props['R1R1']
                del props['R1R2']
                del props['R2R1']
                del props['R2R2']

            if self.SeqBasedSecondStageProps:
                if state == 'S2' or state == 'S3':
                    if self.lastAction['S1'] == 'R1':
                        props['R1'] = self.get_stage2_log_prop(self.propsInS1['R1R1'], self.propsInS1['R1R2'],
                                                               self.propsInS1['R1'], props['R1'], self.InterruptProb)

                        props['R2'] = self.get_stage2_log_prop(self.propsInS1['R1R2'], self.propsInS1['R1R1'],
                                                               self.propsInS1['R1'], props['R2'], self.InterruptProb)

                    if self.lastAction['S1'] == 'R2':
                        props['R1'] = self.get_stage2_log_prop(self.propsInS1['R2R1'], self.propsInS1['R2R2'],
                                                               self.propsInS1['R2'], props['R1'], self.InterruptProb)

                        props['R2'] = self.get_stage2_log_prop(self.propsInS1['R2R2'], self.propsInS1['R2R1'],
                                                               self.propsInS1['R2'], props['R2'], self.InterruptProb)
        return props, {}


    def get_stage2_log_prop(self, cong_seq, incong_seq, stage1action, stage2action, I):
        p = 0
        if I == 0:
            max_term = max([cong_seq, stage1action + stage2action])
        elif I == 1:
            max_term = max([cong_seq + stage2action, incong_seq + stage2action, stage1action + stage2action])
        else:
            max_term = max([cong_seq + stage2action, incong_seq + stage2action, cong_seq, stage1action + stage2action])
        p = max_term + math.log(
            (0.0 if I == 0 else I * math.exp(cong_seq + stage2action - max_term)) +
            (0.0 if I == 0 else I * math.exp(incong_seq + stage2action - max_term)) +
            (0.0 if I == 1 else (-I + 1) * math.exp(cong_seq - max_term)) +
            (math.exp(stage1action + stage2action - max_term))
        )
        max_term = max([cong_seq, incong_seq, stage1action])
        p = p - max_term - math.log(
            math.exp(stage1action - max_term) + math.exp(cong_seq - max_term) + math.exp(incong_seq - max_term))
        return p


    def getActionBias(self, state, action):
        if (state in ['S1', 'S2', 'S3']) and action in ['R1', 'R2']:
            return self.actionBias[state][action]
        return 0.

    def stopLearning(self, state, action):
        return self.notLearnFromIncorrectDiscrimination and (state in ['S2', 'S3']) and (action != 'R1')

    def learn(self, from_state, to_state, action, reward):

        self.lastAction[from_state] = action

        if self.isSeqBased():
            if to_state in self.TerminalStates:
                self.lastOption = self.lastAction['S1'] + action

        if self.stopLearning(from_state, action):
            return

        # adding a bias for common transitions
        # if from_state == 'S1':
        #     self.actionBias['S1']['R1'] = (to_state == 'S2') * self.transitionBias
        #     self.actionBias['S1']['R2'] = (to_state == 'S3') * self.transitionBias

        if from_state == 'S1':
            self.state_bias['S2'] = (to_state == 'S2' and action == 'R1') * self.transitionBias
            self.state_bias['S3'] = (to_state == 'S3' and action == 'R2') * self.transitionBias

        if to_state in self.TerminalStates:
            self.averageReward = (1 - self.averageRewardLR) * self.averageReward + self.averageRewardLR * reward

        if self.isModelBased():
            if from_state != 'S1':
                self.SPE = 1 - self.T[from_state][action][to_state]
                self.T[from_state][action][to_state] += self.SPE * self.transitionLR

                # for performance
                if to_state == 'REWARD':
                    self.T[from_state][action]['NON_REWARD'] *= (1 - self.transitionLR)
                elif to_state == 'NON_REWARD':
                    self.T[from_state][action]['REWARD'] *= (1 - self.transitionLR)
                    # end for performance

                    #                for k, v in self.T[fromState][action].iteritems():
                    #                    if k != toState:
                    #                        self.T[fromState][action][k] *= (1 - self.transitionLR)

        # sequence based learning
        if self.isSeqBased():
            if to_state in self.TerminalStates:
                option = self.lastAction['S1'] + action
                originState = 'S1'
                self.SPE = 1 - self.T[originState][option][to_state]
                self.T[originState][option][to_state] += self.SPE * self.optionLR

                # for performance
                if to_state == 'REWARD':
                    self.T[originState][option]['NON_REWARD'] *= (1 - self.optionLR)
                elif to_state == 'NON_REWARD':
                    self.T[originState][option]['REWARD'] *= (1 - self.optionLR)
                    # end for performance


                # for k, v in self.T[originState][option].iteritems():
                #                    if k != toState:
                #                        self.T[originState][option][k] *= (1 - self.optionLR)

                self.lastOption = option

        # updating Q-values
        if self.isModelFree():
            if from_state == 'S1':
                nextStateValue = max(self.Q[to_state]['R1'], self.Q[to_state]['R2'])
                self.Q['S1'][action] += self.Q1LR * (nextStateValue - self.Q['S1'][action])

            if to_state in self.TerminalStates:
                RPE = reward - self.Q[from_state][action]
                if self.Q[from_state][action] == max(self.Q[from_state]['R1'], self.Q[from_state]['R2']):
                    self.Q['S1'][self.lastAction['S1']] += self.Q1LR * self.ET * RPE
                self.Q[from_state][action] += self.Q2LR * RPE


    def isModelBased(self):
        return self.modelBased

    def isModelFree(self):
        return self.modelFree

    def isSeqBased(self):
        return self.seqBased

    def name(self):
        return 'Seq-Model based'

    @staticmethod
    def extractParameters(config, x):
        params = [double('nan')] * 23
        params[0] = x[0]  # setting beta
        params[18] = params[0]  # setting second stage beta
        params[7] = 0.
        params[8] = 0.
        params[5] = 0.
        params[9] = 0.
        params[10] = 0.
        params[11] = False
        params[12] = 0.
        params[13] = False
        params[14] = 0.
        params[16] = False
        params[19] = 0.
        params[20] = 0.
        params[21] = False
        params[22] = 0.8

        pos = 0
        if ModelConfig.ModelBased in config or ModelConfig.ModelFree in config:
            pos += 1
            params[1] = x[pos]
            params[6] = 1.
        if ModelConfig.SeqBased in config:
            params[2] = params[1]
            params[15] = params[0]
        if ModelConfig.SeqBasedSeparateOptionRL in config:
            pos += 1
            params[2] = x[pos]
        if ModelConfig.ModelFree in config:
            params[3] = params[1]
            params[4] = params[1]
            params[6] = 0
        if ModelConfig.ModelFreeStage1LR in config:
            pos += 1
            params[3] = x[pos]
        if ModelConfig.EligibilityTraces in config:
            pos += 1
            params[5] = x[pos]
        if ModelConfig.EligibilityTracesSetTo1 in config:
            params[5] = 1
        if ModelConfig.ModelFreeModelBasedWeight in config:
            pos += 1
            params[6] = x[pos]
        if ModelConfig.ActionPerseveration in config or \
                        ModelConfig.SeqPerseverationAsActionPerseveration in config:
            pos += 1
            params[7] = x[pos]
        if ModelConfig.SeqPerseveration in config:
            pos += 1
            params[8] = x[pos]
        if ModelConfig.SeqBasedAverageReward in config:
            pos += 1
            params[9] = x[pos]
            pos += 1
            params[10] = x[pos]
        if ModelConfig.SeqBasedSecondStageProps in config:
            params[11] = True
        if ModelConfig.SeqBasedBiased in config:
            pos += 1
            params[12] = x[pos]
        if ModelConfig.InterBased in config:
            pos += 1
            params[13] = True
            params[14] = x[pos]
        if ModelConfig.SeparateSequenceBeta in config:
            pos += 1
            params[15] = x[pos]
        if ModelConfig.SecondStageBeta0 in config:
            params[18] = 0
            if ModelConfig.SepSecondStageBeta in config:
                raise Exception(ModelConfig.SecondStageBeta0 + ' and ' + ModelConfig.SepSecondStageBeta
                                + ' options are incompatible')
        if ModelConfig.SeqPerseverationAsActionPerseveration in config:
            params[8] = params[7]
        if ModelConfig.SequenceWeight1 in config:
            params[16] = True
            params[17] = 1.0
        if ModelConfig.SequenceWeight in config:
            pos += 1
            params[16] = True
            params[17] = x[pos]
        if ModelConfig.SepSecondStageBeta in config:
            pos += 1
            params[18] = x[pos]
        if ModelConfig.DiscriminationBaseline in config:
            pos += 1
            params[19] = x[pos]
        if ModelConfig.TransitionBias in config:
            pos += 1
            params[20] = x[pos]
        if ModelConfig.NotLearnFromIncorrectDiscrimination in config:
            params[21] = True
        if ModelConfig.DynamicStage1Transition in config:
            pos += 1
            params[22] = x[pos]

        return params

    @staticmethod
    def instantiateFromFreeParameters(input_params, config):
        params = LearningModel.extractParameters(config, input_params)

        return LearningModel.instantiateFromParamsAndModel(
            (ModelConfig.ModelBased) in config,
            (ModelConfig.ModelFree) in config,
            (ModelConfig.SeqBased in config),
            len(input_params), params)

    @staticmethod
    def instantiateFromParams(params, config):
        return LearningModel.instantiateFromParamsAndModel(
            (ModelConfig.ModelBased) in config,
            (ModelConfig.ModelFree) in config,
            (ModelConfig.SeqBased in config),
            len(params), params)

    @staticmethod
    def instantiateFromParamsAndModel(modelBased, modelFree, seqBased, df, params):
        return LearningModel(
            modelBased,
            modelFree,
            seqBased,
            df, params[0], params[1], params[2], params[3], params[4], params[5], params[6],
            params[7], params[8], params[9], params[10], params[11], params[12], params[13],
            params[14], params[15], params[16], params[17], params[18], params[19], params[20], params[21], params[22])


    @staticmethod
    def paramNames():
        return ['beta', 'transitionLR', 'optionLR', 'Q1LR', 'Q2LR', 'ET', 'w',
                'actionPerseveration', 'seqPerseveration', 'sequenceValueCoef', 'averageRewardLR',
                'seqBasedSecondStage', 'SeqBias', 'Intrup-based', 'InerruptProp', 'SeqBeta', 'isSequenceWeight',
                'SequenceWeight', 'Second stage beta', 'Discrimination Baseline', 'Transition bias',
                'Ignore wrong discrimination', 'transition']

    def freeParametersCount(self):
        return self.df


    @staticmethod
    def getModel(model, params, config):
        return model.instantiateFromFreeParameters(params, config)

