import time

from model.util import list_powerset, id_generator


class ModelConfig:
    ModelBased = '_MB'
    ModelFree = '_MF'
    SeqBased = '_SB'
    ModelFreeStage1LR = '_S1LR'
    EligibilityTraces = '_ET'
    EligibilityTracesSetTo1 = '_ET1'
    ActionPerseveration = '_AP'
    SeqPerseveration = '_SP'
    SeqPerseverationAsActionPerseveration = '_SPAP'
    ModelFreeModelBasedWeight = '_W'
    SeqBasedSeparateOptionRL = '_OpLR'
    SeqBasedSecondStageProps = '_SPr'
    SeqBasedAverageReward = '_SBAvRe'
    SeqBasedBiased = '_SBBias'
    InterBased = '_Int'
    SeparateSequenceBeta = '_SBe'
    SequenceWeight = '_SW'
    SequenceCoef = '_SCo'
    SepSecondStageBeta = '_S2B'
    DiscriminationBaseline = '_Dsr'
    TransitionBias = '_TBi'
    NotLearnFromIncorrectDiscrimination = '_NLFW'
    DynamicStage1Transition = '_S1Tr'
    SecondStageBeta0 = '_S2B0'
    SequenceWeight1 = '_SW1'

    def get_seq_config(self):
        output = []
        c = list_powerset([
            ModelConfig.SeqBasedSeparateOptionRL,
            ModelConfig.InterBased,
            ModelConfig.SeqBasedSecondStageProps,
            ModelConfig.SepSecondStageBeta,
            ModelConfig.SeparateSequenceBeta,
            ModelConfig.DiscriminationBaseline,
            ModelConfig.TransitionBias
        ])


        for l in c:
            l.append(ModelConfig.ModelBased)
            l.append(ModelConfig.SeqBased)
        output += c

        return output

    def get_mb_mf_config(self):
        output = []
        c = list_powerset(
            [ModelConfig.EligibilityTraces, ModelConfig.ModelFreeStage1LR,
             ModelConfig.SepSecondStageBeta, ModelConfig.DiscriminationBaseline, ModelConfig.TransitionBias
            ])
        for l in c:
            l.append(ModelConfig.ModelBased)
            l.append(ModelConfig.ModelFree)
            l.append(ModelConfig.ModelFreeModelBasedWeight)
        output = output + c
        return output

    def get_mb_config(self):
        output = []
        c = list_powerset(
            [

             ModelConfig.SepSecondStageBeta, ModelConfig.DiscriminationBaseline,
             ModelConfig.TransitionBias

            ])
        for l in c:
            l.append(ModelConfig.ModelBased)
        output = output + c
        return output

    def get_mf_config(self):
        output = []
        c = list_powerset(
            [
                ModelConfig.EligibilityTraces, ModelConfig.ModelFreeStage1LR,
                ModelConfig.SepSecondStageBeta, ModelConfig.DiscriminationBaseline,
                ModelConfig.TransitionBias

            ])
        for l in c:
            l.append(ModelConfig.ModelFree)
        output = output + c
        return output

    def get_mb_or_mf_config(self):
        return self.get_mb_config() + self.get_mf_config() + self.get_mb_mf_config()

    def get_total_config(self):
        return self.get_seq_config() + self.get_mb_or_mf_config()

    @staticmethod
    def get_simple_model():
        return [
            [
                ModelConfig.ModelBased,
                ModelConfig.SeqBased,
                ModelConfig.InterBased,
                ModelConfig.SeqBasedSecondStageProps,
                ModelConfig.SepSecondStageBeta,
                ModelConfig.SeparateSequenceBeta,
            ],
        ]

    @staticmethod
    def get_missing_config():
        return [['_OpLR', '_Int', '_SPr', '_S2B', '_MB', '_SB', '_TBi'], ['_OpLR', '_Int', '_SBe', '_MB', '_SB', '_TBi'], ['_OpLR', '_Int', '_SPr', '_SBe', '_MB', '_SB', '_TBi'], ['_OpLR', '_S2B', '_SBe', '_MB', '_SB', '_TBi'], ['_OpLR', '_Int', '_S2B', '_SBe', '_MB', '_SB', '_TBi'], ['_Int', '_SPr', '_S2B', '_SBe', '_MB', '_SB', '_TBi'], ['_OpLR', '_Int', '_SPr', '_S2B', '_SBe', '_MB', '_SB', '_TBi'], ['_OpLR', '_Int', '_SPr', '_Dsr', '_MB', '_SB', '_TBi'], ['_Int', '_S2B', '_Dsr', '_MB', '_SB', '_TBi'], ['_OpLR', '_Int', '_S2B', '_Dsr', '_MB', '_SB', '_TBi'], ['_OpLR', '_SPr', '_S2B', '_Dsr', '_MB', '_SB', '_TBi'], ['_Int', '_SPr', '_S2B', '_Dsr', '_MB', '_SB', '_TBi'], ['_OpLR', '_Int', '_SPr', '_S2B', '_Dsr', '_MB', '_SB', '_TBi'], ['_OpLR', '_SBe', '_Dsr', '_MB', '_SB', '_TBi'], ['_OpLR', '_Int', '_SBe', '_Dsr', '_MB', '_SB', '_TBi'], ['_OpLR', '_SPr', '_SBe', '_Dsr', '_MB', '_SB', '_TBi'], ['_Int', '_SPr', '_SBe', '_Dsr', '_MB', '_SB', '_TBi'], ['_OpLR', '_Int', '_SPr', '_SBe', '_Dsr', '_MB', '_SB', '_TBi'], ['_S2B', '_SBe', '_Dsr', '_MB', '_SB', '_TBi'], ['_OpLR', '_S2B', '_SBe', '_Dsr', '_MB', '_SB', '_TBi'], ['_Int', '_S2B', '_SBe', '_Dsr', '_MB', '_SB', '_TBi'], ['_OpLR', '_Int', '_S2B', '_SBe', '_Dsr', '_MB', '_SB', '_TBi'], ['_SPr', '_S2B', '_SBe', '_Dsr', '_MB', '_SB', '_TBi'], ['_OpLR', '_SPr', '_S2B', '_SBe', '_Dsr', '_MB', '_SB', '_TBi'], ['_Int', '_SPr', '_S2B', '_SBe', '_Dsr', '_MB', '_SB', '_TBi'], ['_OpLR', '_Int', '_SPr', '_S2B', '_SBe', '_Dsr', '_MB', '_SB', '_TBi'], ['_S1LR', '_Dsr', '_MB', '_MF', '_W', '_TBi'], ['_ET', '_S1LR', '_S2B', '_Dsr', '_MB', '_MF', '_W', '_TBi']]


    @staticmethod
    def getFileName(model, config, timestamp=True, log=''):
        return ''.join(config) + '_' + log + (time.strftime(" %Y-%m-%d %H-%M-%S") if timestamp else '') + '-'+id_generator()


    @staticmethod
    def get_id_config(config, id):
        return ''.join(config) + '_' + id
