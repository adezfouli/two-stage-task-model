import os
from numpy import array
import string
import random

def check_dir_exists(dir_name):
    if not os.path.exists(dir_name):
        os.makedirs(dir_name)

def id_generator(size=4, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


def drange(start, stop, step):
    r = start
    while r < stop:
        yield r
        r += step

def eval_g(x, user_data= None):
    return array([

    ], float_)

nnzj = 0
def eval_jac_g(x, flag, user_data = None):
    if flag:
        return (array([]),
                array([]))

def list_powerset(lst):
    # the power set of the empty set has one element, the empty set
    result = [[]]
    for x in lst:
        # for every additional element in our set
        # the power set consists of the subsets that don't
        # contain this element (just take the previous power set)
        # plus the subsets that do contain the element (use list
        # comprehension to add [x] onto everything in the
        # previous power set)
        result.extend([subset + [x] for subset in result])
    return result

