import csv
import random
import math
from time import time
from model.load_paths import subject_data, ModelClass
from model.model_config import ModelConfig
from util import *


class Environment:
    def __init__(self):
        self.data=[]

    def load_data(self, data):
        self.data = data

    def get_state_reward(self, state, action):
        self.current_trial += 1

        if self.data[self.current_trial].state == 'REWARD':
            reward = 1
        else:
            reward = 0

        return self.data[self.current_trial].state, reward

    def not_finished(self):
        return self.current_trial < self.data.__len__() - 1


    @staticmethod
    def sumulate_from_data(data, modelClass, config, params):
        for i in range(len(data)):
            env = Environment()
            env.load_data(data[i])
            model = modelClass.instantiateFromParams(params, config)
            env.simulate_model(model)


    def simulate_model(self, model):
        initial_state = 'S1'
        self.state = initial_state
        self.current_trial = 0
        self.model_data = []
        self.model = model
        self.NLL = 0
        self.randomLL = 0
        self.choicesCount  = 0

        while self.not_finished():
            self.action = self.get_action()
            self.log_model(self.model)
            (new_state, reward) = self.get_state_reward(self.state, self.action)
            self.model.learn(self.state, new_state, self.action, reward)
            self.state = new_state
            self.skip_repeated_actions()
            # print self.action, self.state

    def skip_repeated_actions(self):
        # skipping multiple key presses during stimulus presentation
        while self.current_trial < self.data.__len__() - 1 and self.data[self.current_trial].state  == self.data[self.current_trial + 1].state:
            self.current_trial += 1

    def export_model_data(self, name, output_path):
        check_dir_exists(output_path)
        path = output_path + name
        output_path = csv.writer(open(path, 'wb'))
        output_path.writerow(['state', 'action',	'state on set', 'action on set', 'action off set', 'trial', 'p_R1', 'p_R2', 'RT_R1', 'RT_R2'])
        for d in self.model_data:
            output_path.writerow(d.to_array())
        return path

    def log_model(self, model):
        props, RT=model.action_probabilities(self.data[self.current_trial].state)

        if self.get_action() != 'auto':
            self.NLL += -props[self.get_action()]
            self.randomLL += -math.log(0.5)
            self.choicesCount += 1

    def get_action(self):
        return self.data[self.current_trial].action

    def nChoices(self):
        return self.choicesCount

    def sample_action(self, p):
        d = random.random()
        total = 0
        for k,v in p.iteritems():
            total += math.exp(v)
            if d < total:
                return k

    @staticmethod
    def test():
        config =             [
                ModelConfig.ModelBased,
                # ModelConfig.ModelFree,
                ModelConfig.SeqBased,
            ]

        # making simulations
        params = [5.0,  #beta
                  1.0,  #transitionLR
                  0.4,  #optionLR
                  1.0,  #Q1LR
                  1.0,  #Q2LR
                  1.0,  #ET
                  0.0,  #w
                  0.0,  #actionPerseveration
                  0.0,  #seqPerseveration
                  0.0,  #sequenceValueCoef
                  0.0,  #averageRewardLR
                  True,  #seqBasedSecondStage
                  0.0,  #SeqBias
                  False,  #Intrup-based
                  0.0,  #InerruptProp
                  5.0,  #SeqBeta
                  None,  #isSequenceWeight
                  None,  #SequenceWeight
                  2.0,  #Second stage beta
                  1.0,  #Discrimination Baseline
                  1.0,  #Transition bias
                  False,  #Ignore wrong discrimination
                  0.7,  #transition
        ]
        path = '../../../data/ID22/test/simulation/'
        start_time = time()
        for i in range(100):
            Environment.sumulate_from_data(subject_data, ModelClass, config, params)
        elapsed_time = time() - start_time
        print elapsed_time


if __name__ == '__main__':
    output_path = '../../data/ID18/simulation/'
    Environment.test()
